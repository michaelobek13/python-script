#Packages
import pandas as pd

def read_csv_file():
    """Getting inputs from files"""
    file = input("Enter the csv file you want to merge [file.csv]: ")
    
    return pd.read_csv(file)

def cleaning_data(csv_input_2):
    """Creates a new data column and re-arranges the columns for merging"""
    # Make a copy of csv_input_2 col_name
    csv_input_2['column_name_original'] = csv_input_2['column_name']

    # Convert csv_input_2 col_name to lowercase
    csv_input_2['column_name'] = csv_input_2['column_name'].apply(lambda x: x.lower())
    
    # Reorder csv_input_2 columns
    csv_input_new = csv_input_2[['column_name', 'column_name_original', 'id']]

    return csv_input_new

def merge_file(csv_input_1, csv_input_new):
    """ This function merges the columns based 
        on the column names and outputs it into
        a new csv file
    """

    # Merge on col_name
    merge_data = csv_input_1.merge(csv_input_new, on = 'column_name')

    # Rename columns of resulting datasheet
    merge_data.columns = ['id', 'column_name', 'col_name', 'id_']

    # Save merged data
    merge_data.to_csv("csv_output.csv", index = False)

    return merge_data

def main():
    
    # Get inputs from csv files
    csv_input_1 = read_csv_file()
    csv_input_2 = read_csv_file()
    
    #Cleaning the data
    csv_input_clean = cleaning_data(csv_input_2)
    
    #Merging inputs based on their column_names
    merge_file(csv_input_1, csv_input_clean)

if __name__ == "__main__":
    main()
