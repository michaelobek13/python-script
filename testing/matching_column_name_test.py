import pytest
import pandas as pd

from matching_column_name import read_csv_file, cleaning_data, merge_file

@pytest.mark.clean()
def test_clean_data():
    """The data will be cleaned and arranged to be used for merging"""
    test_input = pd.read_csv("../python-script/files/Test_Input_2.csv")
    test_desired_output = pd.read_csv("../python-script/files/Test_Desired_Clean_Data.csv")

    assert cleaning_data(test_input).values.all() == test_desired_output.values.all()

@pytest.mark.merge
def test_merge_file():
    """Merges two csv files by column name"""
    input_1 = pd.read_csv("../python-script/files/Test_Input_1.csv")
    input_2 = pd.read_csv("../python-script/files/Test_Input_2.csv")
    desired_output = pd.read_csv("../python-script/files/Test_Desired_Output.csv")

    #Gets cleaned data for merging function
    new_input_2 = cleaning_data(input_2)

    assert merge_file(input_1, new_input_2).values.all() == desired_output.values.all()

@pytest.mark.skip
def test_length_of_file():
    """Length of each input file is equal to each other."""
    inputfile_1 = "Test_Input_1.csv"
    inputfile_2 = "Test_Input_2.csv"
    assert len(read_csv_file(inputfile_1)) == len(read_csv_file(inputfile_2))

@pytest.mark.skip
def test_equal_headers():
    """Headers of each file are equal to each other"""
    test_inputfile_1 = read_csv_file("Test_Input_1.csv")
    test_inputfile_2 = read_csv_file("Test_Input_2.csv")
    assert test_inputfile_1.columns.all() == test_inputfile_2.columns.all()

@pytest.mark.skip
def test_equal_column():
    """The column values are equal to each other"""
    test_dataframe = pd.read_csv("Test_Input_1.csv")
    test_dataframe['column_name_original'] = test_dataframe['column_name']
    assert test_dataframe['column_name_original'].values.all() == test_dataframe['column_name'].values.all()
