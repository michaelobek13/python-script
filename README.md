<h1> MatchingColumnNameScript </h1>

<h2>Script Description</h2>
    <p>This script takes inputs from csv files and matches the column names of each file. Then, the matched column names would then be placed into a new csv file.</p>

<h2>Prerequisites</h2>
    <p>You required to install the requirements.txt file which contains the pandas package and its dependencies.</p>
    <p>To install the project's requirements, open up a terminal and enter the command:
        pip install -r requirements.txt</p>

<h2>Testing</h2>

<h3>How to run tests</h3>
    <p>To perform tests, you would only need to add your own input files (it must be a csv file). You may also use the input files included in the repo as a reference.</p>
    <p>To add your files for test, you can add it into the files folder. After inputting your files, open up the terminal and open the directory where your script is located. </p>
    <p>To run the tests, enter: pytest {scriptname.py}</p>





